//
//  HIltonClass.swift
//  TestTimeDeleteMe
//
//  Created by Mark Vader on 2/6/15.
//  Copyright (c) 2015 VaderApps.com. All rights reserved.
//

import UIKit
import Foundation
import CoreMedia
import CoreMedia.CMTime


var someTime : CMTime = CMTimeMakeWithSeconds(2, 600)

public class HiltonTimeStruct : NSObject {
    
    init(time : Double) {
        self.hiltonTime = CMTimeMakeWithSeconds(time, 600)
    }
    var hiltonTime : CMTime
    
    
}

public protocol HiltonTimeProtocol  {
    var hiltonTimeComposed : HiltonTimeStruct {get set}
}


public class HiltonClass: NSObject {
   
}
