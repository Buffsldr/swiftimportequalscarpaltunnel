//
//  HiltonTimeFramework.h
//  HiltonTimeFramework
//
//  Created by Mark Vader on 2/6/15.
//  Copyright (c) 2015 VaderApps.com. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HiltonTimeFramework.
FOUNDATION_EXPORT double HiltonTimeFrameworkVersionNumber;

//! Project version string for HiltonTimeFramework.
FOUNDATION_EXPORT const unsigned char HiltonTimeFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HiltonTimeFramework/PublicHeader.h>


